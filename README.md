# Estimating SARS-Cov-2 outbreak basic reproduction number by Autonomous community in Spain

Data source: https://covid19.isciii.es/

Stuff presented in [notebooks/1.0-modeling_rt_spain.ipynb](https://gitlab.com/fpozoc/covid-rt/-/blob/master/notebooks/1.0-modeling_rt_spain.ipynb).

Initially forked from: https://github.com/k-sys/covid-19/blob/master/Realtime%20R0.ipynb